# argo-app

CMS Recast Effort cluster manifests:

- [argo-cd](/argo-cd) contains the [Argo CD](https://argoproj.github.io/argo-cd/) manifests including kustomizations.
- [argo](/argo-manifests/) contains the [Argo Workflows](https://github.com/argoproj/argo/blob/master/manifests/) manifests including kustomizations.

Each app directory is split into subdirectories:

- `base` contains the base manifests for a base installation including references to upstream manifests.
- if applicable, `overlays/production` contains specific patches/overlays of resources in `base`.
- `secrets`: contains secrets encrypted with SOPS (Barbican option) as well as generators to decrypt them using kustomize-SOPS (KSOPS).
